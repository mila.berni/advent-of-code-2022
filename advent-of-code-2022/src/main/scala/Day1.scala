import scala.io.Source

object Day1 extends App {
  val filename = "day-1.txt"
  val source = Source.fromResource(filename)
  val lines: Array[String] = source.getLines.toArray

  var calories: List[Int] = List.empty[Int]
  val linesIterator = lines.iterator
  var sum = 0

  while (linesIterator.hasNext) {
    var nextValue: String = linesIterator.next()
    var nextValueInt: Int = {
      if (nextValue == "")
        -1
      else
        nextValue.toInt
    }

    if (nextValueInt == -1) {
      calories = calories :+ sum
      sum = 0
    } else {
      sum += nextValueInt
    }
  }

  calories = calories :+ sum

  // answer to first problem
  println(calories.max)

  calories = calories.sortWith(_ > _)

  // answer to second problem
  println(calories.take(3).sum)

  source.close()
}