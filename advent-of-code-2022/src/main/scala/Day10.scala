import scala.collection.mutable.ListBuffer

object Day10 extends App {
  val lines = Utils.readFile("day-10.txt")

  var cycleCounter = 0
  var register = 1
  val cycles = collection.mutable.Map.empty[Int, Int]
  cycles += (0 -> register)

  def noop(): Unit = {
    cycles += (cycleCounter + 1 -> register)
    cycleCounter += 1
  }

  def add(X: Int): Unit = {
    cycles += (cycleCounter + 1 -> register)
    cycles += (cycleCounter + 2 -> (register + X))
    cycleCounter += 2
    register += X
  }

  lines.foreach(line => {
    line.contains("noop") match {
      case true => noop()
      case false => val X = line.split(" ").last.toInt; add(X)
    }
  })

  var sum = 0
  for (i <- 20 to cycleCounter by 40) {
    sum += i * cycles(i-1)
  }

  // Part One
  println(sum)

  println()
  // Part Two

  val lit = "#"
  val dark = "."
  val CRT = collection.mutable.Map.empty[Int, ListBuffer[String]]

  for (i <- 0 to 5) {
    val line = ListBuffer.empty[String]
    for (j <- 0 to 39) {
      line += dark
    }
    CRT += (i -> line)
  }


  for (row <- 0 until 6) {
    for (column <- 0 until 40) {
      val cycle = (row * 40 + column)
      val X = cycles(cycle)
      val sprite = Array[Int](X - 1, X, X + 1)
      if (sprite.contains(column)) CRT(row)(column) = lit
    }
  }

  prettyPrint(CRT)

  private def printSprite(sprite: Array[Int]): Unit = {
    for (i <- 0 until 40) {
      if (sprite.contains(i)) print(lit)
      else print(dark)
    }
    println()
  }

  private def prettyPrint(CRT: collection.mutable.Map[Int, ListBuffer[String]]): Unit = {
    println()
    for (row <- 0 to 5) {
      for (pixel <- 0 to 39) {
        print(CRT(row)(pixel))
      }
      println()
    }
    println()
  }
}
