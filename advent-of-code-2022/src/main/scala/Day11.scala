import scala.collection.mutable.ListBuffer

object Day11 extends App {
  val lines = Utils.readFile("day-11.txt")

  val monkeysLines = collection.mutable.Map.empty[Int, ListBuffer[String]]
  val monkeys = collection.mutable.Map.empty[Int, Monkey]

  var buffer = ListBuffer.empty[String]
  var i = 0

  lines.foreach(line => {
    if (line == "") {
      monkeysLines += (i -> buffer)
      i += 1
      buffer = ListBuffer.empty[String]
    }
    else buffer += line.trim()
  })

  monkeysLines += (i -> buffer)

  monkeysLines.values.foreach(monkeyLines => {
    val monkey = parseMonkey(monkeyLines)
    monkeys += (monkey.num -> monkey)
  })

  var ROUNDS = 20

  var inspections = collection.mutable.Map.empty[Int, Long]
  monkeys.foreach(m => {
    inspections += (m._1 -> 0)
  })

  for (i <- 0 until ROUNDS) {
    for (monkeyId <- 0 until monkeys.size) {
      val monkey = monkeys(monkeyId)
      for (j <- monkey.items.indices) {
        inspections(monkeyId) += 1
        val itemAfterOp = monkey.operation.execute(monkey.items(j))
        val itemAfterMonkeyIsBored = Item(itemAfterOp.worryLevel / 3)
        if (monkey.test.condition(itemAfterMonkeyIsBored)) monkey.throwItem(itemAfterMonkeyIsBored, monkeys(monkey.monkeyIfTrue))
        else monkey.throwItem(itemAfterMonkeyIsBored, monkeys(monkey.monkeyIfFalse))
      }
      monkey.items = ListBuffer.empty[Item]
    }
  }

  println(inspections.values.toList.sortWith(_ > _).take(2).product)

  // Part Two
  println()

  monkeysLines.values.foreach(monkeyLines => {
    val monkey = parseMonkey(monkeyLines)
    monkeys += (monkey.num -> monkey)
  })

  ROUNDS = 10000

  inspections = collection.mutable.Map.empty[Int, Long]
  monkeys.foreach(m => {
    inspections += (m._1 -> 0)
  })

  var factor = 1
  monkeys.values.foreach(m => {
    factor *= m.divisibleBy
  })


  for (i <- 0 until ROUNDS) {
    for (monkeyId <- 0 until monkeys.size) {
      val monkey = monkeys(monkeyId)
      for (j <- monkey.items.indices) {
        inspections(monkeyId) += 1
        val itemAfterOp = monkey.operation.execute(monkey.items(j))
        val itemAfterMonkeyIsBored = Item(itemAfterOp.worryLevel % factor)
        if (monkey.test.condition(itemAfterMonkeyIsBored)) monkey.throwItem(itemAfterMonkeyIsBored, monkeys(monkey.monkeyIfTrue))
        else monkey.throwItem(itemAfterMonkeyIsBored, monkeys(monkey.monkeyIfFalse))
      }
      monkey.items = ListBuffer.empty[Item]
    }
  }

  println(inspections.values.toList.sortWith(_ > _).take(2).product)



  def parseMonkey(lines: ListBuffer[String]): Monkey = {
    val monkeyNum = parseMonkeyNum(lines(0))
    val items = parseItems(lines(1))
    val operation = parseOperation(lines(2))
    val test = parseTest(lines(3))
    val monkeyTrue = parseAction(lines(4))
    val monkeyFalse = parseAction(lines(5))
    val divisibleBy = lines(3).split(" ").last.toInt

    Monkey(monkeyNum, items, operation, test, divisibleBy, monkeyTrue, monkeyFalse)
  }

  private def parseMonkeyNum(line: String): Int = line.replace(":", "").split(" ").last.toInt

  private def parseItems(line: String): ListBuffer[Item] = {
    val numbersArray = line.split(":").last.replace(" ", "")
    val numbers = numbersArray.split(",").map(num => num.toInt)
    val result = ListBuffer.empty[Item]
    numbers.foreach(num => result.append(Item(num)))
    result
  }

  private def parseOperation(line: String): Operation = {
    object MonkeyOperation extends Operation {
      override def execute(item: Item): Item = {
        val operation = line.split(":").last.trim().split("=").last.trim()
        val secondEl = operation.split(" ").last

        operation.contains("*") match {
          case true =>
            if (secondEl.equals("old")) Item(item.worryLevel * item.worryLevel)
            else Item(item.worryLevel * secondEl.toInt)
          case false =>
            if (secondEl.equals("old")) Item(item.worryLevel + item.worryLevel)
            else Item(item.worryLevel + secondEl.toInt)
        }
      }
    }

    MonkeyOperation
  }

  private def parseTest(line: String): Test = {
    object MonkeyTest extends Test {
      override def condition(item: Item): Boolean = {
        val number = line.split(" ").last.toInt
        item.worryLevel % number == 0
      }
    }

    MonkeyTest
  }

  private def parseAction(line: String): Int = {
    line.split(" ").last.toInt
  }

}

case class Item(worryLevel: Long)

trait Operation {
  def execute(item: Item): Item
}

trait Test {
  def condition(item: Item): Boolean
}

case class Monkey(num: Int, var items: ListBuffer[Item], operation: Operation, test: Test, divisibleBy: Int, monkeyIfTrue: Int, monkeyIfFalse: Int) {
  def throwItem(item: Item, other: Monkey): Unit = {
    other.items += item
  }
}
