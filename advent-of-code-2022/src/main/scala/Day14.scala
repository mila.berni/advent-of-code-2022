import scala.collection.mutable
import scala.collection.mutable.ListBuffer

object Day14 extends App {
  val lines = Utils.readFile("day-14.txt")

  val rocks = mutable.HashSet.empty[Point]

  lines.foreach(line => {
    val rocksInLine = ListBuffer.empty[Point]
    val pointsInLine = line.split("->").map(point => point.trim())
    pointsInLine.foreach(pointInLine => {
      val rock = pointInLine.split(",").map(p => p.toInt)
      val newPoint = Point(rock.head, rock(1))
      rocksInLine += newPoint
    })
    for (i <- 1 until rocksInLine.length) {
      rocksInLine(i).getPointsBetween(rocksInLine(i - 1)).foreach(r => rocks += r)
    }
  })


  // Part One
  val sandUnits = mutable.HashSet.empty[Point]
  val cave = Cave(rocks, sandUnits)

  var totUnits = 0

  while (simulateSand(cave.sandStart))
    totUnits += 1

  println(totUnits)

  println()

  // Part Two
  while (simulateSandOnTheFloor(cave.sandStart)) {
    totUnits += 1
  }

  println(totUnits)


  def simulateSand(start: Point): Boolean = {

    if (start.y > cave.getDeepestRock()) {
      false
    } else {
      if (cave.pointIsEmpty(moveDown(start))) {
        simulateSand(moveDown(start))
      } else if (cave.pointIsEmpty(moveLeft(start))) {
        simulateSand(moveLeft(start))
      } else if (cave.pointIsEmpty(moveRight(start))) {
        simulateSand(moveRight(start))
      } else {
        cave.sandUnits += start
        true
      }
    }
  }

  def simulateSandOnTheFloor(start: Point): Boolean = {

    if (!cave.pointIsEmpty(cave.sandStart)) {
      false
    } else {
      if (cave.pointIsEmptyAndNotFloor(moveDown(start))) {
        simulateSandOnTheFloor(moveDown(start))
      } else if (cave.pointIsEmptyAndNotFloor(moveLeft(start))) {
        simulateSandOnTheFloor(moveLeft(start))
      } else if (cave.pointIsEmptyAndNotFloor(moveRight(start))) {
        simulateSandOnTheFloor(moveRight(start))
      } else {
        cave.sandUnits += start
        true
      }
    }
  }

  private def moveDown(point: Point): Point = {
    Point(point.x, point.y + 1)
  }

  private def moveLeft(point: Point): Point = {
    moveDown(Point(point.x - 1, point.y))
  }

  private def moveRight(point: Point): Point = {
    moveDown(Point(point.x + 1, point.y))
  }

}

case class Point(x: Int, y: Int) {
  def getPointsBetween(other: Point): mutable.ListBuffer[Point] = {
    val result = mutable.ListBuffer.empty[Point]

    if (x.equals(other.x) && y < other.y) for (j <- y to other.y) result += Point(x, j)
    else if (x.equals(other.x) && y > other.y) for (j <- other.y to y) result += Point(x, j)
    else if (x > other.x && y.equals(other.y)) for (i <- other.x to x) result += Point(i, y)
    else if (x < other.x && y.equals(other.y)) for (i <- x to other.x) result += Point(i, y)

    result
  }
}

case class Cave(rocks: mutable.HashSet[Point], sandUnits: mutable.HashSet[Point]) {

  val sandStart = Point(500, 0)

  def pointIsEmpty(target: Point): Boolean = {
    !rocks.contains(target) && !sandUnits.contains(target)
  }

  def pointIsEmptyAndNotFloor(target: Point): Boolean = {
    !rocks.contains(target) && !sandUnits.contains(target) && !isFloor(target)
  }

  def isFloor(target: Point): Boolean = {
    target.y == getDeepestRock + 2
  }

  def getDeepestRock(): Int = {
    rocks.foldLeft(0)((result, point) => if (point.y > result) point.y else result)
  }

  def prettyPrint(): Unit = {
    val minX: Int = rocks.foldLeft(sandStart.x)((result, rock) => {
      if (rock.x < result) rock.x
      else result
    })

    val maxX: Int = rocks.foldLeft(sandStart.x)((result, rock) => {
      if (rock.x > result) rock.x
      else result
    })

    val minY: Int = rocks.foldLeft(sandStart.y)((result, rock) => {
      if (rock.y < result) rock.y
      else result
    })

    val maxY: Int = rocks.foldLeft(sandStart.y)((result, rock) => {
      if (rock.y > result) {
        rock.y
      }
      else result
    })

    for (j <- minY to maxY) {
      for (i <- minX to maxX) {
        if (rocks.contains(Point(i, j))) print("#")
        else if (Point(i, j).equals(sandStart)) print("+")
        else if (sandUnits.contains(Point(i, j))) print("o")
        else print('.')
      }
      println()
    }
  }
}
