import scala.collection.mutable.ListBuffer

object Day15 extends App {
  val lines = Utils.readFile("day-15.txt")

  val sensors = collection.mutable.ListBuffer.empty[Sensor]

  lines.foreach(line => {
    val sensorLine = line.split(":").head.replace("Sensor at", "").trim().replace(" ", "")
    val beaconLine = line.split(":").last.replace("closest beacon is at", "").trim().replace(" ", "")

    val sensorX = sensorLine.split(",").head.split("=").last.toInt
    val sensorY = sensorLine.split(",").last.split("=").last.toInt

    val beaconX = beaconLine.split(",").head.split("=").last.toInt
    val beaconY = beaconLine.split(",").last.split("=").last.toInt

    sensors += Sensor(Location(sensorX, sensorY), Location(beaconX, beaconY))
  })

  val targetY = 10

  val intervals = ListBuffer.empty[Interval]

  sensors.foreach(sensor => {
    if (sensor.coversRow(targetY)) intervals += sensor.getLimitsOfRow(targetY)
  })

  println(mergeIntervals(intervals).foldLeft(0)((sum, interval) => sum + (interval.rightLimit - interval.leftLimit)))

  def mergeIntervals(intervals: ListBuffer[Interval]): List[Interval] = {
    intervals.sortBy(_.leftLimit).foldLeft(List.empty[Interval]) { (acc, r) =>
      acc match {
        case head :: tail if head.contains(r) =>
          head :: tail
        case head :: tail if head.contains(r.leftLimit) =>
          Interval(head.leftLimit, r.rightLimit) :: tail
        case _ =>
          r :: acc
      }
    }
  }

  println()

  // Part Two

  def isCovered(target: Location): Boolean = {
    sensors.foreach(sensor => {
      if (sensor.coversLocation(target)) return true
    })

    false
  }

  def hasPointNotCovered(sensor: Sensor, lowerLimit: Int = 0, upperLimit: Int = 20): Unit = {
    var diffX = 0
    var reachedFarthestLeft = false

    for (y <- sensor.farthestTopLocation.y to sensor.farthestDownLocation.y) {
      val newXLeft = sensor.farthestTopLocation.x + diffX
      val newXRight = sensor.farthestTopLocation.x - diffX

      if (y >= lowerLimit && y <= upperLimit
        && newXLeft >= lowerLimit && newXLeft <= upperLimit
        && newXRight >= lowerLimit && newXRight <= upperLimit
      ) {
        val nextLeftLocation = Location(newXLeft, y)
        val nextRightLocation = Location(newXRight, y)

        if (nextLeftLocation == sensor.farthestLeftLocation) reachedFarthestLeft = true

        if (!reachedFarthestLeft) diffX -= 1
        else diffX += 1

        if (!isCovered(nextLeftLocation))
          println(s"Found point not covered @ $nextLeftLocation from sensor $sensor")
        else if (!isCovered(nextRightLocation))
          println(s"Found point not covered @ $nextRightLocation from sensor $sensor")
        else ""
      }
    }
  }

  val lowerLimit = 0
  val upperLimit = 4000000

  sensors.foreach(s => hasPointNotCovered(s, lowerLimit, upperLimit))

  println()

  println(4000000.toLong * 3257428.toLong + 2573243.toLong)

}

case class Sensor(sensor: Location, beacon: Location) {

  val manhattanDistanceFromBeacon: Int = (sensor.x - beacon.x).abs + (sensor.y - beacon.y).abs

  val farthestTopLocation = Location(sensor.x, sensor.y - manhattanDistanceFromBeacon - 1)
  val farthestDownLocation = Location(sensor.x, sensor.y + manhattanDistanceFromBeacon + 1)
  val farthestRightLocation = Location(sensor.x + manhattanDistanceFromBeacon + 1, sensor.y)
  val farthestLeftLocation = Location(sensor.x - manhattanDistanceFromBeacon - 1, sensor.y)

  def manhattanDistanceFrom(target: Location): Int = (sensor.x - target.x).abs + (sensor.y - target.y).abs

  def coversLocation(target: Location): Boolean = manhattanDistanceFrom(target) <= manhattanDistanceFromBeacon

  def coversRow(targetY: Int): Boolean = {
    if ((sensor.y - targetY).abs > manhattanDistanceFromBeacon) false
    else true
  }

  def getLimitsOfRow(targetY: Int): Interval = {
    if (!coversRow(targetY)) throw new IllegalArgumentException

    val leftXLimit = sensor.x - (manhattanDistanceFromBeacon - (targetY - sensor.y).abs)
    val rightXLimit = sensor.x + (manhattanDistanceFromBeacon - (targetY - sensor.y).abs)

    Interval(leftXLimit, rightXLimit)
  }
}

case class Location(x: Int, y: Int) {
  def isBetween(loc1: Location, loc2: Location): Boolean = {
    if (loc1.isBefore(loc2)) this.isAfter(loc1) && this.isBefore(loc2)
    else this.isBefore(loc1) && this.isAfter(loc2)
  }

  def isAfter(other: Location): Boolean = x >= other.x
  def isBefore(other: Location): Boolean = !isAfter(other)
  def isBelow(other: Location): Boolean = y >= other.y
  def isAbove(other: Location): Boolean = !isBelow(other)
}

case class Interval(leftLimit: Int, rightLimit: Int) {
  assert(leftLimit <= rightLimit)

  def contains(rhs: Interval) = leftLimit <= rhs.leftLimit && rhs.rightLimit <= rightLimit

  def contains(v: Int) = leftLimit <= v && v <= rightLimit
}
