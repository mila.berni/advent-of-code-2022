
object Day2 extends App {
  val lines = Utils.readFile("day-2.txt")
  val iterator = lines.iterator

  var symbols = Map(
    "A" -> "Rock",
    "B" -> "Paper",
    "C" -> "Scissors",
    "X" -> "Rock",
    "Y" -> "Paper",
    "Z" -> "Scissors"
  )

  var sum = 0
  while (iterator.hasNext) {
    val round = iterator.next().split(" ")
    sum += checkWin(symbols.get(round(1)).get, symbols.get(round(0)).get)
  }

  // answer to first problem
  println(sum)
  println()


  sum = 0
  val newIterator = lines.iterator
  while (newIterator.hasNext) {
    val round = newIterator.next().split(" ")
    val opponentMove = symbols.get(round(0)).get
    val desiderata = round(1)
    var myMove = ""

    if (opponentMove == "Rock") {
      if (desiderata == "X") myMove = "Scissors" // LOSE
      else if (desiderata == "Y") myMove = "Rock" // DRAW
      else if (desiderata == "Z") myMove = "Paper" // WIN
    } else if (opponentMove == "Paper") {
      if (desiderata == "X") myMove = "Rock" // LOSE
      else if (desiderata == "Y") myMove = "Paper" // DRAW
      else if (desiderata == "Z") myMove = "Scissors" // WIN
    } else if (opponentMove == "Scissors") {
      if (desiderata == "X") myMove = "Paper" // LOSE
      else if (desiderata == "Y") myMove = "Scissors" // DRAW
      else if (desiderata == "Z") myMove = "Rock" // WIN
    }

    sum += checkWin(myMove, opponentMove)
  }

  // answer to second problem
  println(sum)
  println()

  private def checkWin(me: String, opponent: String): Int = {
    var result = 0

    if (me == "Rock") {
      result += 1
      if (opponent == "Rock") result += 3
      else if (opponent == "Paper") result += 0
      else if (opponent == "Scissors") result += 6
    } else if (me == "Paper") {
      result += 2
      if (opponent == "Rock") result += 6
      else if (opponent == "Paper") result += 3
      else if (opponent == "Scissors") result += 0
    } else if (me == "Scissors") {
      result += 3
      if (opponent == "Rock") result += 0
      else if (opponent == "Paper") result += 6
      else if (opponent == "Scissors") result += 3
    }

    result
  }
}
