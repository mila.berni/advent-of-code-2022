import scala.util.matching.Regex

object Day21 extends App {
  val lines = Utils.readFile("day-21.txt")

  val numbers = lines.foldLeft(Map.empty[String, String])((map, line) => {
    map + (line.split(": ")(0) -> line.split((": "))(1))
  })

  def getValue(key: String, numbers: Map[String, String]): Long = {
    val value = numbers(key)

    if (value.forall(Character.isDigit)) value.toLong
    else {
      val equationAsList = value.split(" ")
      val firstMonkey = getValue(equationAsList(0), numbers)
      val operation = equationAsList(1)
      val secondMonkey = getValue(equationAsList(2), numbers)

      operation match {
        case "+" => firstMonkey + secondMonkey
        case "-" => firstMonkey - secondMonkey
        case "*" => firstMonkey * secondMonkey
        case "/" => firstMonkey / secondMonkey
      }
    }
  }

  // Part One
  println(getValue("root", numbers) + "\n")


  // Part Two
  val expressions = numbers("root").split(" ")
  val secondExpr = getValue(expressions.last, numbers) // there's no human in the second term
  val HUMAN = "humn"
  var target = secondExpr.toDouble

  def findX(key: String, numbers: Map[String, String]): Expression = {
    val value = numbers(key)

    var newExpression = Expression(None, None)

    if (key == HUMAN) newExpression = Expression(Option(1L.toDouble), None)
    else if (value.forall(Character.isDigit)) newExpression = Expression(None, Option(value.toDouble))
    else {
      val equationAsList = value.split(" ")
      val firstMonkey = findX(equationAsList.head, numbers)
      val operation = equationAsList(1)
      val secondMonkey = findX(equationAsList.last, numbers)

      if (firstMonkey.isHuman || secondMonkey.isHuman || firstMonkey.isBinomial || secondMonkey.isBinomial)
        println(s"$firstMonkey$operation$secondMonkey = $target")

      if (firstMonkey.isHuman && secondMonkey.isNumber) {
        if (operation == "+")
          newExpression = Expression(firstMonkey.coefficient, secondMonkey.number)
        else
          newExpression = Expression(firstMonkey.coefficient, Option(-secondMonkey.number.get))
      } else if (firstMonkey.isNumber && secondMonkey.isHuman) {
        if (operation == "+")
          newExpression = Expression(secondMonkey.coefficient, firstMonkey.number)
        else
          newExpression = Expression(Option(-secondMonkey.coefficient.get), firstMonkey.number)
      } else if (firstMonkey.isNumber && secondMonkey.isBinomial) {
        var newCoefficient = secondMonkey.coefficient.get
        var newNumber = secondMonkey.number.get
        operation match {
          case "+" => newNumber += firstMonkey.number.get
          case "-" => newNumber -= firstMonkey.number.get
          case "*" =>
            newNumber *= firstMonkey.number.get
            newCoefficient *= firstMonkey.number.get
          case "/" => target *= firstMonkey.number.get
        }
        newExpression = Expression(Option(newCoefficient), Option(newNumber))
      } else if (firstMonkey.isBinomial && secondMonkey.isNumber) {
        var newCoefficient = firstMonkey.coefficient.get
        var newNumber = firstMonkey.number.get
        operation match {
          case "+" => newNumber += secondMonkey.number.get
          case "-" => newNumber -= secondMonkey.number.get
          case "*" =>
            newNumber *= secondMonkey.number.get
            newCoefficient *= secondMonkey.number.get
          case "/" => target *= secondMonkey.number.get
        }
        newExpression = Expression(Option(newCoefficient), Option(newNumber))
      } else if (firstMonkey.isNumber && secondMonkey.isNumber) {
        var result = 0L.toDouble
        operation match {
          case "+" => result = firstMonkey.number.get + secondMonkey.number.get
          case "-" => result = firstMonkey.number.get - secondMonkey.number.get
          case "*" => result = firstMonkey.number.get * secondMonkey.number.get
          case "/" => result = firstMonkey.number.get / secondMonkey.number.get
        }
        newExpression = Expression(None, Option(result))
      } else throw new IllegalArgumentException
    }

    if (newExpression.isBinomial) {
      val divisor = gcd(target.abs, gcd(newExpression.coefficient.get.abs, newExpression.number.get.abs))
      target /= divisor
      Expression(Option(newExpression.coefficient.get / divisor), Option(newExpression.number.get / divisor))
    } else newExpression
  }

  def gcd(a: Double, b: Double): Double = {
    if (a == 0) return b
    if (b == 0) return a

    if (a > b) {
      gcd(b, a % b)
    }
    else gcd(a, b % a)
  }

  def getEquation(key: String, numbers: Map[String, String]): String = {
    val value = numbers(key)

    if (key == HUMAN) "x"
    else if (value.forall(Character.isDigit)) value
    else {
      val equationAsList = value.split(" ")
      val firstTerm = getEquation(equationAsList(0), numbers)
      val operation = equationAsList(1)
      val secondTerm = getEquation(equationAsList(2), numbers)

      if (firstTerm.forall(Character.isDigit) && secondTerm.forall(Character.isDigit)) {
        operation match {
          case "+" => (firstTerm.toDouble + secondTerm.toDouble).toString
          case "-" => (firstTerm.toDouble - secondTerm.toDouble).toString
          case "*" => (firstTerm.toDouble * secondTerm.toDouble).toString
          case "/" => (firstTerm.toDouble / secondTerm.toDouble).toString
        }
      } else s"($firstTerm $operation $secondTerm)"
    }
  }


  println(s"Start target = $target\n")
  val humanValue = findX(expressions.head, numbers)
  println(s"\n$humanValue = $target")

  println((8.1945852958067552E16.toLong + 1.34300711340563E14.toLong) / 23085)

}

case class Expression(coefficient: Option[Double], number: Option[Double]) {
  def isHuman: Boolean = coefficient.isDefined && number.isEmpty
  def isNumber: Boolean = !isHuman
  def isBinomial: Boolean = coefficient.isDefined && number.isDefined
}