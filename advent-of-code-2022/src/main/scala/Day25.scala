import scala.annotation.tailrec
import scala.collection.immutable.ListMap

object Day25 extends App {
  val lines = Utils.readFile("day-25.txt")
  val BASE = 5

  def convertToDecimal(number: String): BigInt = {
    number.reverse.zipWithIndex.foldLeft(BigInt(0L))((result, tuple) => {
      val (digit, power): (Int, Int) =
        if (tuple._1 == '-') (-1, tuple._2)
        else if (tuple._1 == '=') (-2, tuple._2)
        else (tuple._1.toInt - 48, tuple._2)
      result + BigInt(digit * math.pow(BASE, power).toLong)
    })
  }

  @tailrec
  def getMaxExponent(number: BigInt, currentExp: Int = 1): Int = {
    if (number.abs < BASE) return 0
    val result = number.abs / BASE
    if (result > BASE) getMaxExponent(result, currentExp + 1)
    else currentExp
  }

  def convertToSNAFU(number: BigInt, currentConversions: Map[BigInt, Decomposition], powers: Set[Power]): Option[Map[BigInt, Decomposition]] = {
    if (powers.contains(Power(BASE, 0))) Option(currentConversions)
    else {
      var power = Power(BASE, getMaxExponent(number))
      var decomposition = Decomposition(number, power, (number / power.value).toInt)
      if (decomposition.times > 2) {
        if (!powers.contains(decomposition.power.copy(exponent = power.exponent + 1))) {
          power = power.copy(exponent = power.exponent + 1)
          decomposition = decomposition.copy(power = power, times = 1)
        }
        else return None
      }

      var result = convertToSNAFU(decomposition.getRest.abs, currentConversions + (number -> decomposition), powers + power)
      var newDecomposition = decomposition.copy(times = decomposition.times + 1)
      while (result.isEmpty) {
        if (newDecomposition.times > 2) {
          if (!powers.contains(newDecomposition.power.copy(exponent = power.exponent + 1))) {
            val newPower = power.copy(exponent = power.exponent + 1)
            newDecomposition = newDecomposition.copy(power = newPower, times = 1)
          } else return result
        }
        else {
          result = convertToSNAFU(newDecomposition.getRest.abs, currentConversions + (number -> newDecomposition), powers + power)
          newDecomposition = newDecomposition.copy(times = newDecomposition.times + 1)
        }
      }
      result
    }
  }

  def fromDecompositionToSNAFU(currentKey: BigInt, decompositions: Map[BigInt, Decomposition], signum: Int): String = {
    var result = ""
    val currentDecomposition = decompositions(currentKey)
    if (signum >= 0) result = currentDecomposition.times.toString
    else if (signum < 0 && currentDecomposition.times == 1) result = "-"
    else if (signum < 0 && currentDecomposition.times == 2) result = "="

    if (decompositions.contains(currentDecomposition.getRest.abs)) {
      val nextDecomposition = decompositions(currentDecomposition.getRest.abs)
      if (currentDecomposition.power.exponent - nextDecomposition.power.exponent > 1) {
        val zeroToFill = "0" * (currentDecomposition.power.exponent - nextDecomposition.power.exponent - 1)
        result += zeroToFill
      }
    }

    if (currentDecomposition.power.exponent != 0)
      result + fromDecompositionToSNAFU(currentDecomposition.getRest.abs, decompositions, signum * currentDecomposition.getRest.signum)
    else result
  }

  val sum = lines.foldLeft(BigInt(0L))((result, number) => result + convertToDecimal(number))
  println(sum + "\n")
  println()

  val result = convertToSNAFU(sum, Map.empty[BigInt, Decomposition], Set.empty[Power])
  if (result.isDefined) {
    val stringInSNAFU = fromDecompositionToSNAFU(sum, result.get, sum.signum)
    println(stringInSNAFU)
  }
}

case class Power(base: Int, exponent: Int) {
  def value: BigInt = BigInt(math.pow(base, exponent).toLong)
}

case class Decomposition(number: BigInt, power: Power, times: Int) {
  lazy val getRest: BigInt = number - power.value * times
}
