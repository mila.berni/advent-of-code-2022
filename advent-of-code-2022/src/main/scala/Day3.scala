
object Day3 extends App {
  // Parto One
  val lines = Utils.readFile("day-3.txt")
  var sum = 0
  lines.foreach(elem => {
    val firstHalf = elem.substring(0, elem.length / 2)
    val secondHalf = elem.substring(elem.length / 2, elem.length)
    var done = false
    for (char <- firstHalf if !done)
      if (secondHalf.contains(char)) {
        if (char.isLower) sum += char.toInt - 96
        else if (char.isUpper) sum += char.toInt - 38
        done = true
      }
  }
  )
  println(sum)

  // Part Two
  sum = 0
  for(i <- lines.indices by 3) {
    var done = false
    for (char <- lines(i) if !done)
      if (lines(i+1).contains(char) && lines(i+2).contains(char)) {
        if (char.isLower) sum += char.toInt - 96
        else if (char.isUpper) sum += char.toInt - 38
        done = true
      }
  }
  println(sum)
}
