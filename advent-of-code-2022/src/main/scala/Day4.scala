object Day4 extends App {
  val FIRST_START = 0
  val FIRST_END = 1
  val SECOND_START = 2
  val SECOND_END = 3

  def parseLine(line: String): Array[Int] = {
    line.split(",").flatMap(x => x.split("-")).map(x => x.toInt)
  }

  def fullyContain(assignment: Array[Int]): Boolean = {
    if (assignment(SECOND_START) <= assignment(FIRST_START) && assignment(FIRST_END) <= assignment(SECOND_END)) true
    else if (assignment(FIRST_START) <= assignment(SECOND_START) && assignment(SECOND_END) <= assignment(FIRST_END)) true
    else false
  }

  def overlaps(assignment: Array[Int]): Boolean = {
    if (assignment(SECOND_START) <= assignment(FIRST_START) && assignment(FIRST_END) <= assignment(SECOND_END)) true
    else if (assignment(FIRST_START) <= assignment(SECOND_START) && assignment(SECOND_END) <= assignment(FIRST_END)) true
    else if (assignment(FIRST_START) <= assignment(SECOND_START) && assignment(SECOND_START) <= assignment(FIRST_END)) true
    else if (assignment(SECOND_START) <= assignment(FIRST_START) && assignment(FIRST_START) <= assignment(SECOND_END)) true
    else false
  }

  val lines = Utils.readFile("day-4.txt").map(parseLine)

  // Part One
  var count = 0
  lines.foreach(x => if (fullyContain(x)) count += 1)
  println(count)

  // Part Two
  count = 0
  lines.foreach(x => if (overlaps(x)) count += 1)
  println(count)
}
