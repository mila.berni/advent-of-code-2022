import scala.collection.mutable.ListBuffer

object Day5 extends App {

  case class Move(crates: Int, fromStack: Int, toStack: Int)

  val lines = Utils.readFile("day-5.txt")
  val moves = ListBuffer[Move]()
  val stacks = ListBuffer[List[String]]()
  val step = 4

  lines.foreach( line =>
    if (line.trim().contains("move")) {
      val split = line.split(" ")
      moves += Move(split(1).toInt, split(3).toInt, split(5).toInt)
    } else if (line.trim().nonEmpty) {
      val temp = new ListBuffer[String]()
      for (i <- 0 to line.length by step) {
        if (i+step > line.length) {
          temp += line.substring(i, line.length).trim()
        }
        else {
          temp += line.substring(i, i+4).trim()
        }
      }
      stacks += temp.toList
    }
  )

  val map = collection.mutable.Map.empty[Int, ListBuffer[String]]

  val maxLen = {
    var max = 0
    stacks.foreach(stack =>
      if (stack.length > max) max = stack.length
    )
    max
  }

  for (i <- stacks.indices)
    if (stacks(i).length < maxLen) stacks(i) = stacks(i) :+ ""

  for (stack <- stacks.reverse.transpose)
    map += (stack.head.toInt -> stack.slice(1, stack.length).filter(value => value.nonEmpty))

  for (move <- moves) {
    for (i <- 0 until move.crates) {
      map(move.toStack) += map(move.fromStack).last
      map(move.fromStack).remove(map(move.fromStack).length-1)
    }
  }

  // Part One
  for (i <- 1 to maxLen) {
    print(map(i).last.replace("[", "").replace("]", ""))
  }

  println()
  println()


  // Part Two
  val newMap = collection.mutable.Map.empty[Int, ListBuffer[String]]
  for (stack <- stacks.reverse.transpose)
    newMap += (stack.head.toInt -> stack.slice(1, stack.length).filter(value => value.nonEmpty))

  for (move <- moves) {
    newMap(move.fromStack).takeRight(move.crates).foreach(el => newMap(move.toStack).append(el))
    newMap(move.fromStack).remove(newMap(move.fromStack).length - move.crates, move.crates)
  }

  for (i <- 1 to maxLen) {
    print(newMap(i).last.replace("[", "").replace("]", ""))
  }

  println()
}
