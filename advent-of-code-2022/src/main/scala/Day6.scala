import util.control.Breaks._

object Day6 extends App {
  val line = Utils.readFile("day-6.txt").head

  // To solve part two, change this value from 4 to 14
  val messageLen = 4

  def findDistinct(sequence: String): Boolean = {
    for (i <- 0 until sequence.length)
      if (sequence.substring(i + 1, sequence.length).contains(sequence(i))) return false
    true
  }

  for (i <- 0 to line.length - messageLen) {
    if (findDistinct(line.substring(i, i+messageLen))) {
      println(i + messageLen)
      break
    }
  }

}
