import scala.collection.mutable.ListBuffer

object Day7 extends App {
  val lines = Utils.readFile("day-7.txt")

  case class Folder(name: String, files: ListBuffer[File], subFolders: ListBuffer[Folder])
  case class File(size: Int, name: String)

  val folders = collection.mutable.Map.empty[String, ListBuffer[String]]

  var currentFolder = ""

  var buffer = ListBuffer.empty[String]

  var backCounter = 0

  lines.foreach(line => {

    if (line.contains("$ cd") && !line.contains("..")) {
      if (currentFolder.nonEmpty) {
        folders += (currentFolder.trim() -> buffer)
        buffer = ListBuffer.empty[String]
      }
      if (backCounter > 0) {
        for (i <- 0 until backCounter) {
          currentFolder = currentFolder.substring(0, currentFolder.lastIndexOf("/"))
        }
        backCounter = 0
      }

      if (!line.split(" ").last.equals("/")) {
        if (!currentFolder.equals("/"))
          currentFolder += "/" + line.split(" ").last
        else
          currentFolder += line.split(" ").last
      } else
        currentFolder = "/"
    } else if (line.contains("dir") || line.split(" ").head.forall(_.isDigit)) {
      buffer += line
    } else if (line.contains("$ cd .."))
      backCounter += 1

  })

  folders += (currentFolder -> buffer)

//  folders.foreach(println)
//  println()

  val sizes = collection.mutable.Map.empty[String, Int]

  folders.foreach(folder => {
    sizes += (folder._1 -> findSum(folder._1))
  })

  def findSum(folder: String): Int = {
    var sum = 0
    folders(folder).foreach(el => {
      if (el.contains("dir")) {
        val subDir = el.split(" ").last
        if (folder.equals("/"))
          sum += findSum("/" + subDir)
        else
          sum += findSum(folder + "/" + subDir)
      } else {
        val dim = el.split(" ").head.toInt
        sum += dim
      }
    })
    sum
  }

//  println()
//  sizes.foreach(println)

  var sum = 0
  sizes.foreach(size => {
    if (size._2 <= 100000) sum += size._2
  })

  // Part One
  println(sum)
  println()

  // Part Two
  val total = 70000000
  val required = 30000000

  val toDelete = required - (total - sizes("/"))

  val deletions = ListBuffer.empty[Int]

  sizes.foreach(size => {
    if (toDelete - size._2 < 0) {
      deletions += size._2
    }
  })

  println(deletions.sortWith(_ < _).head)
}
