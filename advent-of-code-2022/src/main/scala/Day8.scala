import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks._

object Day8 extends App {

  val lines = Utils.readFile("day-8.txt")

  val forest = Forest(ListBuffer.empty[ListBuffer[Tree]])

  lines.foreach(line => {
    val row = ListBuffer.empty[Tree]
    val positions = line.split(("")).map(_.toInt)
    positions.foreach(pos => {
      row += Tree(pos)
    })
    forest.trees += row
  })

  var visibleTrees = forest.perimeter

  for (row <- 1 until forest.height - 1) {
    for (column <- 1 until forest.width - 1) {
      val canBeSeen = forest.canBeSeen(row, column)
      if (canBeSeen) visibleTrees += 1
    }
  }

  // Part One
  println(visibleTrees)
  println()

  // Part Two
  var viewingScores = ListBuffer.empty[Int]

  for (row <- 0 until forest.height; column <- 0 until forest.width) {
    viewingScores += forest.getViewingScore(row, column)
  }

  println(viewingScores.max)
}

case class Forest(trees: ListBuffer[ListBuffer[Tree]]) {
  def getTreeAt(row: Int, column: Int): Tree = {
    trees(row)(column)
  }

  def height: Int = trees.length
  def width: Int = trees.head.length

  def perimeter: Int = (height * 2) + (width - 2) * 2

  def canBeSeen(row: Int, column: Int): Boolean = {
    val targetTree = getTreeAt(row, column)
    val hiddenMap = collection.mutable.Map.empty[Int, Boolean]

    for (i <- 0 until height if i != row) {
      if (i < row) if (getTreeAt(i, column).isGreaterEqual(targetTree)) hiddenMap += (0 -> true)
      if (i > row) if (getTreeAt(i, column).isGreaterEqual(targetTree)) hiddenMap += (1 -> true)
    }

    for (j <- 0 until width if j != column) {
      if (j < column) if (getTreeAt(row, j).isGreaterEqual(targetTree)) hiddenMap += (2 -> true)
      if (j > column) if (getTreeAt(row, j).isGreaterEqual(targetTree)) hiddenMap += (3 -> true)
    }

    hiddenMap.size < 4
  }

  def getViewingScore(row: Int, column: Int): Int = {
    val targetTree = getTreeAt(row, column)

    var leftScore = 0
    var rightScore = 0
    var upScore = 0
    var downScore = 0

    breakable {
      for (i <- row - 1 to 0 by -1) { //UP
        val nextTree = getTreeAt(i, column)
        upScore += 1
        if (nextTree.isGreaterEqual(targetTree)) {
          break
        }
      }
    }

    breakable {
      for (i <- row + 1 until height) { // DOWN
        val nextTree = getTreeAt(i, column)
        downScore += 1
        if (nextTree.isGreaterEqual(targetTree)) {
          break
        }
      }
    }

    breakable {
      for (j <- column - 1 to 0 by -1) { // LEFT
        val nextTree = getTreeAt(row, j)
        leftScore += 1
        if (nextTree.isGreaterEqual(targetTree)) {
          break
        }
      }
    }

    breakable {
      for (j <- column + 1 until width) { //RIGHT
        val nextTree = getTreeAt(row, j)
        rightScore += 1
        if (nextTree.isGreaterEqual(targetTree)) {
          break
        }
      }
    }

    rightScore * leftScore * upScore * downScore
  }
}

case class Tree(height: Int) {
  def isGreaterEqual(other: Tree): Boolean = {
    height >= other.height
  }

  def equals(other: Tree): Boolean = {
    height == other.height
  }

  def isGreater(other: Tree): Boolean = {
    height > other.height
  }
}
