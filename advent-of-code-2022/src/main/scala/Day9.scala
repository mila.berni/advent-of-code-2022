import scala.collection.mutable.HashSet
import scala.collection.mutable.ListBuffer

object Day9 extends App {
  val lines = Utils.readFile("day-9.txt")

  val moves = ListBuffer.empty[Move]
  val start = Spot(0, 0)

  lines.foreach(line => {
    val direction = line.split(" ").head
    val steps = line.split(" ").last.toInt
    for (i <- 0 until steps)
      moves += Move(direction)
  })

  val RIGHT = "R"
  val LEFT = "L"
  val UP = "U"
  val DOWN = "D"


  private def moveKnot(knot: Spot, move: Move): Spot = {
    move.direction match {
      case RIGHT => Spot(knot.row, knot.column + 1)
      case UP => Spot(knot.row - 1, knot.column)
      case LEFT => Spot(knot.row, knot.column - 1)
      case DOWN => Spot(knot.row + 1, knot.column)
    }
  }

  private def moveTail(head: Spot, tail: Spot): Spot = {
    val distance = tail.distantTo(head)
    var result = tail

    if (distance >= 2) {
      (head.row == tail.row, head.column == tail.column) match {
        case (true, false) =>
          if (head.column > tail.column) result = moveKnot(tail, Move(RIGHT))
          else result = moveKnot(tail, Move(LEFT))
        case (false, true) =>
          if (head.row > tail.row) result = moveKnot(tail, Move(DOWN))
          else result = moveKnot(tail, Move(UP))
        case (true, true) => result
        case (false, false) =>
          if (distance > 2) {
            (head.row > tail.row, head.column > tail.column) match {
              case (true, true) => result = moveKnot(moveKnot(tail, Move(RIGHT)), Move(DOWN))
              case (true, false) => result = moveKnot(moveKnot(tail, Move(LEFT)), Move(DOWN))
              case (false, true) => result = moveKnot(moveKnot(tail, Move(RIGHT)), Move(UP))
              case (false, false) => result = moveKnot(moveKnot(tail, Move(LEFT)), Move(UP))
            }
          }
      }
    }

    result
  }

  val headPositions, tailPositions = HashSet.empty[Spot]
  headPositions += start
  tailPositions += start

  var head, tail = start


  for (i <- moves.indices) {
    head = moveKnot(head, moves(i))
    tail = moveTail(head, tail)

    headPositions += head
    tailPositions += tail
  }

  // Part One
  println(tailPositions.size)
  println()


  // Part Two
  val HEAD = 0
  val ropePositions = collection.mutable.Map.empty[Int, collection.mutable.HashSet[Spot]]
  val knotsPosition = collection.mutable.Map.empty[Int, Spot]

  for (i <- 0 to 9) {
    ropePositions += (i -> collection.mutable.HashSet.empty[Spot])
    knotsPosition += (i -> Spot(0, 0))
  }

  knotsPosition(HEAD)

  for (i <- moves.indices) {
    knotsPosition(HEAD) = moveKnot(knotsPosition(HEAD), moves(i))
    for (knot <- 1 to 9) {
      knotsPosition(knot) = moveTail(knotsPosition(knot-1), knotsPosition(knot))
      ropePositions(knot) += knotsPosition(knot)
    }
  }

  println(ropePositions(9).size)

}

case class Spot(row: Int, column: Int) {
  def equals(other: Spot): Boolean = {
    row == other.row && column == other.column
  }

  def distantTo(other: Spot): Int = {
    if (row == other.row) (column - other.column).abs
    else if (column == other.column) (row - other.row).abs
    else (column - other.column).abs + (row - other.row).abs
  }
}

case class Move(direction: String)
