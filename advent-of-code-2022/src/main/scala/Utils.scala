import scala.io.Source
import scala.util.{Failure, Success, Try}

object Utils {
  def readFile(filename: String): Array[String] = {
    Try(Source.fromResource(filename).getLines) match {
      case Success(lines) => lines.toArray.map(line => line.replace("\n", ""))
      case Failure(e) =>
        println(e.printStackTrace())
        Array.empty[String]
    }
  }
}
